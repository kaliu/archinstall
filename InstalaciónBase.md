 # Instalación de Arch Linux

Bienvenido a mi guía de instalación de *Arch Linux*. Esta guía esta escrita en markdown y posteriormente convertida a HTML para su lectura. Esta guía, ademas, cuenta con links a la [wiki de Arch](https://wiki.archlinux.org) por si quieres saber mas informacion sobre una palabra o concepto.



En esta guía asumo que:

- Vas a instalar *Arch Linux* sobre un sistema con soporte UEFI
- Vas a usar la versión de *Arch Linux* de 64bits
- Tienes conocimientos basicos sobre *Linux* y comandos
- Tienes conexión a Internet



Si cumples estos requisitos podemos empezar.



[TOC]



## 1. Creación y formateo de particiones

Antes de empezar necesitamos crear las particiones donde se instalará nuestro sistema.

Hay varias formas de crearlas, en esta guía usaremos **cfdisk** pero tu puedes crearlas de la forma que prefieras(usando por ejemplo otro gestor de particiones por terminal o uno con interfaz, las particiones a crear serán las mismas). A continuación voy a numerar las particiones que podemos crear y algunas propiedades y funciones de las mismas:

| Nombre de la partición                            | Punto de montaje | Totalmente necesaria? | Sistema de archivos                                          | Tamaño                                               | Función                                                      |
| ------------------------------------------------- | :--------------- | --------------------- | ------------------------------------------------------------ | ---------------------------------------------------- | ------------------------------------------------------------ |
| Root                                              | /                | Sí                    | Puede ser cualquiera soportado: [ext4](https://wiki.archlinux.org/index.php/Ext4)(default) o [btrfs](https://wiki.archlinux.org/index.php/Btrfs), [jfs](https://wiki.archlinux.org/index.php/JFS), [xfs](https://wiki.archlinux.org/index.php/XFS), [reiserfs](https://en.wikipedia.org/wiki/ReiserFS), etc. | El que tu necesites                                  | Es la partición raíz del sistema y el punto de comienzo de todo el sistema de archivos. |
| EFI partition                                     | /boot/EFI        | Sí                    | FAT32                                                        | 300Mb                                                | Contiene los archivos necesarios para que inicie el sistema. |
| Home                                              | /home            | No                    | Normalmente el mismo que el de la /, pero puede ser cualquier otro. | El que tu necesites                                  | Es posible separar las homes en una partición distinta pero no es totalmente necesario. |
| [Swap](https://wiki.archlinux.org/index.php/Swap) | No tiene         | No                    | Swap                                                         | Es recomendable un tamaño similar al doble de tu RAM | Útil en sistemas con poca memoria actualmente no es totalmente necesaria pero en algunas ocasiones recomendada. |

En este apartado mostraré como hacerlo con cfdisk pero no debería haber mucho problema si quieres hacerlas con otra herramienta.

Antes de empezar con eso vamos a cargar el teclado español para no tener problemas a la hora de poner símbolos.

`loadkeys es`



Ahora si, para ejecutar cfdisk deberemos ejecutar:

`cfdisk /dev/sdX` 

(Donde sdX es el disco donde quieres instalar Arch)



En esta interfaz nos movemos con las flechas y el enter. Al ejecutar el comando nos saldrá una pantalla parecida a esta:

![Imagen del tipo de sistema de particiones](imgs/img001.png)

Seleccionaremos **gpt** y pulsaremos *ENTER*.

Y apareceremos en la siguiente ventana:

![](imgs/img002.png)

A partir de ahora simplemente seleccionamos *New* y pulsamos enter.



Nos preguntará el tamaño, en este caso es la partición de EFI por lo que le diremos que la queremos de *300M*

![](imgs/img003.png)

Pulsamos *ENTER* y volveremos a la pantalla de principal. Movemos el cursor a *TYPE* para cambiar el tipo de partición y pulsamos *ENTER*.

Movemos el cursor hacia arriba del todo y nos posicionamos donde pone EFI System.

![](imgs/img004.png)

Y pulsamos *ENTER* y veremos que ya se muestra como *EFI System*

![](imgs/img005.png)



Ahora vamos a crear la partición root. El proceso es exactamente el mismo si queremos dividir otras carpetas del sistema en otras particiones (/home, /etc).

Creamos una nueva partición del tamaño que queramos. Si no queremos ninguna partición más ni swap el plan es llenar el resto del disco con la partición del root. En caso de queramos alguna de estas particiones calcularemos el espacio que le queremos dar a cada una.

En este caso no vamos a crear ninguna partición más a parte de la root y tendremos una SWAP de 1Gb.



Para crear la partición root nos moveremos con la flecha hacia abajo al espacio libre y pulsaremos de nuevo en *NEW*. Seleccionamos de nuevo el tamaño de la partición. A esta no le vamos a modificar el  tipo ya que por defecto se pone en Linux Filesystem y es justo el que necesitamos. A estas alturas nuestra pantalla tiene que parecerse a esto:

![](imgs/img006.png)



Finalmente vamos a crear la partición de *SWAP*. Seleccionamos de nuevo el espacio libre y creamos la partición con el espacio restante.

Nos movemos a *TYPE* y pulsamos *ENTER*. Esta vez vamos a seleccionar el tipo *Linux Swap*.

![](imgs/img007.png)

Pulsamos *ENTER* y ya tenemos nuestro sistema de particiones configurado.

Por lo tanto, y para dejarlo claro, nuestra pantalla deberia ser algo parecido a esto:

![](imgs/img008.png)

Tenemos una partición EFI, la partición root y la partición de la Swap.

Si lo anterior es correcto, podemos proceder a escribir los cambios en el disco moviéndonos a la opción *WRITE* y pulsando *ENTER*. Nos va a pedir confirmación, debemos escribir *yes* y volver a pulsar *ENTER*.

Si todo sale bien nos saldrá abajo un texto en azul diciendonos que la tabla de particiones ha sido alterada. Nos moveremos a la opción *QUIT* para salir del programa y pulsamos *ENTER*.



Volveremos a nuestra querida terminal. Podemos revisar si las particiones se han escrito en el disco con el siguiente comando:

`fdisk -l`

La salida de ese comando debería parecerse a esto:

![](imgs/img009.png)

De nuevo veremos la partición EFI, la del sistema de archivos Linux y la Swap. Debemos recordar los identificadores (/dev/sdXX) para poder continuar con la instalación. En nuestro caso, y para simplificarlo, usaremos los nombres de nuestra tabla de particiones que tampoco es probable que difiera mucho de la tuya.

Por lo tanto vamos a proceder a formatear las particiones.



Empezamos con la EFI que vamos a formatear con Fat32

`mkfs.fat -F32 /dev/sda1`

Que devolverá lo siguiente:

![](imgs/img010.png)





La partición root en ext4 (como ya hemos comentado se pueden formatear con otros sistemas de archivos pero para eso deberás buscar información en la [Wiki de Arch](https://wiki.archlinux.org/) ya que en esta guía se usará ext4)

`mkfs.ext4 /dev/sda2`

Que devolverá lo siguiente 

![](imgs/img011.png)





Y la swap con el comando:

`mkswap /dev/sda3`

Que devolverá lo siguiente:

![](imgs/img012.png)

Y con esto nos hemos sacado el tema de las particiones de encima :)



## 2. Instalación de los paquetes base

Para comenzar con la instalación de los paquetes básicos necesitaremos montar la partición root en /mnt. La montaremos con el siguiente comando:

`mount /dev/sda2 /mnt`

Además deberemos 'encender' la partición swap:

`swapon /dev/sda2`

Si ninguno de estos dos comandos nos da error estamos preparados para instalar el sistema base de Arch Linux con el siguiente comando:

`pacstrap /mnt base base-devel`

Esta es la parte mas larga de la instalación ya que consiste en descargar todos los paquetes que son necesarios para que funcione el sistema y además instalarlos en el disco. Dependiendo de la potencia del ordenador y la velocidad de la conexión puede durar entre 5 y 40 minutos. 

Mientras el proceso continúe y no suelte ningún error tendremos que tener paciencia hasta que acabe.





Ahora deberemos decirle a [fstab](https://wiki.archlinux.org/index.php/Fstab) que queremos montar esa partición al inicio con el siguiente comando:

`genfstab -U -p /mnt >> /mnt/etc/fstab`

Para comprobar que se ha escrito correctamente le haremos un cat al archivo

![E](imgs/img013.jpg)

Como podemos ver las particiones con esas UUID se montarán respectivamente como root y swap.





## 3. Configuración básica del sistema

Ahora deberemos acceder a nuestra instalación de Arch. Para esto usaremos la herramienta [arch-chroot](https://wiki.archlinux.org/index.php/Chroot#Using_arch-chroot)

`arch-chroot /mnt `

Al ejecutar el comando el prompt cambiará a algo parecido a esto:

![](imgs/img014.jpg)

Desde aqui podemos modificar toda la configuración como si estuviermos en el sistema iniciado.

Lo primero que deberiamos hacer es darle un nombre a la maquina.

`ècho "NOMBRE" > /etc/hostname`

Donde *NOMBRE* es el nombre que le queremos poner.

A continuación deberemos establecer nuestro idioma y otros ajustes de localización

Vamos a modificar el archivo */etc/locale.gen* y vamos a descomentar la linea referente al Español. En el caso del Español de España es es_ES pero para otros países de Latinoamerica también hay especificaciones (suele ser es_ y el nombre del país acortado). Para modificar este archivo usaremos nano

`nano /etc/locale.gen`

Para descomentar simplemente borramos la almohadilla que hay al principio de la linea como se muestra en el ejemplo:

![](imgs/img015.jpg)

Guardamos con *CONTROL + O* y salimos usando *CONTROL + X*. Para aplicar estos cambios ejecutamos

`locale-gen`

Debería devolver algo de este estilo

![](imgs/img016.jpg)

Y para acabar de generar el layout ejecutaremos los dos siguientes comandos

`echo LANG=es_ES.UTF-8 > /etc/locale.conf`

`export LANG=es_ES.UTF-8`

Repito que yo uso la variante de España pero es tan sencillo como cambiar el codigo de pais.

El siguiente paso es establecer la zona horaria. Los archivos se encuentran en */usr/share/zoneinfo/CONTINETE/CIUDAD* donde *CONTINENTE* es el continente donde te encuentres y *CIUDAD* es la ciudad importante más cercana que tengas.

Cuando hayas localizado el tuyo creamos un enlace con el siguiente comando al archivo de configuración del sistema

`ln -s /usr/share/zoneinfo/Europe/Madrid /etc/localtime`

Y estableceremos el reloj de hardware

`hwclock --systohc --utc`



Ahora  vamos a crear el usuario que vamos a usar en el sistema.

Para añadirlo ejecutamos

`useradd -mg users -G wheel,storage,power -s /bin/bash USUARIO`

Donde USUARIO es el nombre de usuario que queramos



Ahora debemos establecer la contraseña de este usuario con el comando passwd

`passwd USUARIO`

Donde USUARIO es el usuario creado. Te va a pedir introducir la contraseña 2 veces. Si la pones bien te dirá que la contraseña se ha actualizado correctamente.

Tambien deberemos establecer la contraseña del root. Para ello ejecutamos simplemente

`passwd`



Vamos a tener que modificar un archivo para que nuestro usuario pueda usar el comando sudo. Este archivo se encuentra en */etc/sudoers*. Lo modificaremos con nano

`nano /etc/sudoers`

Des-comentamos la linea donde pone *%wheel ALL=(ALL) ALL*

![](imgs/img017.jpg)





## 4. Instalación de GRUB y arranque del sistema 

Para poder arrancar el sistema deberemos instalar GRUB en la partición de EFI pero antes deberemos instalar unos paquetes.

Vamos a actualizar los repositorios con el comando:

`pacman -Sy`

Y, ahora si, vamos a instalar los paquetes para el GRUB y otras herramientas

`pacman -S grub efibootmgr dosfstools os-prober mtools`

![](imgs/img018.png)

Creamos la carpeta de EFI

`mkdir /boot/EFI`

Y montamos la partición EFI en esa carpeta con

`mount /dev/sda1 /boot/EFI`



Finalmente instalamos GRUB con el siguiente comando

`grub-install --target=x86_64-efi  --bootloader-id=grub_uefi --recheck`

Y si todo ha salido bien nos dará una salida como esta

![](imgs/img019.png)

Y para finalizar creamos la configuración de GRUB con el comando:

`grub-mkconfig -o /boot/grub/grub.cfg`

Con su correspondiente salida correcta

![](imgs/img020.png)





Y en principio ya lo tenemos todo instalado.

Solo nos queda salir del chroot:

`exit`

Desmontar correctamente todas las particiones montadas

`umount -a`

Y reiniciar el sistema:

`reboot`



Felicidades! Tu sistema Arch Linux ha sido instalado con éxito!



------

Webgrafía:  [TecMint](https://www.tecmint.com/arch-linux-installation-and-configuration-guide/), [Tom's Hardware Forum](https://forums.tomshardware.com/faq/how-to-install-arch-linux-with-uefi.1544105/) y, como no, la maravillosa [Wiki de Arch Linux](https://wiki.archlinux.org/)

------

Un proyecto hecho por kaliu con :heart: y mucha :musical_note: ​

